from copy import deepcopy
# from typing import List
from PIL import Image


def elevations_line_to_str(line):
    rez = ""

    for tup in line:
        rez += str(tup) + " "

    return rez


def elevations_asc_to_relief_image(asc_path, image_name):
    file = open(asc_path, "r")
    print("\n Reading file for relief image")
    elevations_matrix = []
    i = 0
    for line in file:
        i += 1
        if i%100 ==0:
            print("loaded line ", i)
        elevations_line = parse_line(line)
        if not elevations_line:
            print("Jumped over line: '" + line + "'")
            continue
        elevations_matrix.append(elevations_line)

    rgb_tuple = []

    l = len(elevations_matrix)
    i = 0
    for line in elevations_matrix:
        if i % 100 == 0:
            print("computing relief line" + str(i) + "/" + str(l))
        rgb_tuple += elevations_to_rgb(line)
        i += 1

    img = Image.new('RGB', (len(elevations_matrix[0]), len(elevations_matrix)))
    img.putdata(rgb_tuple)
    img.save(image_name + '.png')
    print("Saved elevation image!")

def parse_line(line_string):
    line_string = line_string.strip()
    elements = line_string.split(" ")
    if len(elements) < 100: return []
    digital_line = []
    for element in elements:
        element = element.strip()
        digital_line.append(float(element))

    return digital_line


def elevations_to_rgb(elevations):
    rgb_tuple_list = []
    for elevation in elevations:
        rgb_tuple_list.append(elevation_to_rgb(elevation))
    return rgb_tuple_list


def elevation_to_rgb(elevation):
    """
    elevation   color          rgb
    0 - 300     green         (151,210,140)
    300 - 800   yellow        (241,233,172)
    800 - 1500  yellow-brown  (219,207,167)
    1500 - 2500 light brown   (184,166,124)
    2500 - 3500 brown         (148,130,93)
    3500 - 5000 dark brown    (102,89,64)
    >5000       grey          (201,198,193)

    0 - -300      white       (227,253,253)
    -300 - -800   white-blue  (188,223,241)
    -800 - -1500  light blue  (154,209,237)
    -1500 - -2500 blue        (130,195,229)
    -2500 - -3500 aqua        (90,167,207)
    -3500 - -5000 dark aqua   (47,129,172)
    <-5000        deep blue   (8,78,114)
    """

    if elevation is None: return 0, 0, 0
    # elif elevation < 0: return 8, 78, 114
    elif elevation < -10: return 90, 167, 207
    elif -10 <= elevation < 0: return 154, 209, 237
    # elif -5000 <= elevation < -3500: return 47, 129, 172
    # elif -3500 <= elevation < -2500: return 90, 167, 207
    # elif -2500 <= elevation < -1500: return 130, 195, 229
    # elif -1500 <= elevation < -800: return 154, 209, 237
    # elif -800 <= elevation < -300: return 188, 223, 241
    elif elevation == 0: return 227, 253, 253

    elif 0 < elevation < 50: return 190, 225, 180
    elif 50 <= elevation < 250: return 170, 220, 160
    elif 250 <= elevation < 500: return 151, 210, 140
    elif 500 <= elevation < 750: return 200, 220, 158
    elif 750 <= elevation < 1000: return 241, 233, 172

    elif 1000 <= elevation < 1250: return 235, 225, 171
    elif 1250 <= elevation < 1500: return 230, 218, 170
    elif 1500 <= elevation < 1750: return 225, 211, 169
    elif 1750 <= elevation < 2000: return 219, 207, 167

    elif 2000 <= elevation < 2250: return 209, 197, 157
    elif 2250 <= elevation < 2500: return 199, 187, 147
    elif 2500 <= elevation < 2750: return 189, 177, 137
    elif 2750 <= elevation < 3000: return 184, 166, 124

    elif 3000 <= elevation < 3250: return 176, 157, 116
    elif 3250 <= elevation < 3500: return 168, 148, 108
    elif 3500 <= elevation < 3750: return 160, 139, 100
    elif 3750 <= elevation < 4000: return 148, 130, 93

    elif 4000 <= elevation < 4250: return 137, 120, 86
    elif 4250 <= elevation < 4500: return 126, 110, 79
    elif 4500 <= elevation < 4750: return 115, 100, 72
    elif 4750 <= elevation < 5000: return 102, 89, 64

    elif 5000 <= elevation < 5250: return 112, 99, 74
    elif 5250 <= elevation < 5500: return 122, 109, 84
    elif 5500 <= elevation < 5750: return 132, 119, 94
    elif 5750 <= elevation < 6000: return 142, 129, 104

    elif 6000 <= elevation < 6250: return 152, 139, 114
    elif 6250 <= elevation < 6500: return 162, 149, 124
    elif 6500 <= elevation < 6750: return 172, 159, 134
    elif 6750 <= elevation < 7000: return 182, 169, 144

    elif elevation >= 7000: return 201, 198, 193

    # elif 0 < elevation < 125: return 151, 210, 140
    # elif 125 <= elevation < 250: return 196, 222, 156
    #
    # elif 250 <= elevation < 500: return 241, 233, 172
    # elif 500 <= elevation < 750: return 234, 224, 170
    # elif 750 <= elevation < 1000: return 227, 215, 168
    #
    # elif 1000 <= elevation < 1250: return 219, 207, 167
    # elif 1250 <= elevation < 1500: return 207, 193, 153
    # elif 1500 <= elevation < 1750: return 195, 179, 139
    #
    # elif 1750 <= elevation < 2000: return 184, 166, 124
    # elif 2000 <= elevation < 2250: return 172, 154, 114
    # elif 2250 <= elevation < 3000: return 160, 142, 104
    #
    # elif 3000 <= elevation < 3250: return 148, 130, 93
    # elif 3250 <= elevation < 3500: return 133, 117, 83
    # elif 3500 <= elevation < 3750: return 118, 104, 73
    #
    # elif 3750 <= elevation < 4000: return 102, 89, 64
    # elif 4000 <= elevation < 4250: return 122, 111, 90
    # elif 4250 <= elevation < 4500: return 142, 133, 116
    # elif 4500 <= elevation < 4750: return 162, 155, 142
    # elif 4750 <= elevation < 5000: return 182, 177, 168
    #
    # elif elevation > 5000: return 201, 198, 193
    else:
        print(elevation)
        return 0, 0, 0


def ruggedpoint_to_rgb(mean_elevation):
    """
    rugedness   color          rgb
    0 - 10      green         (124,219,135) - basically flat
    10 - 50     white-gray    (226,234,219) - a bit hilly
    50 - 100    light-gray    (210, 210, 189) - tall hills
    100 - 250   gray          (160, 162, 146) - tame mountains
    250 - 500   dark gray     (95, 95, 95) - mountains
    500 - 750   black         (46, 46, 46) - tall mountains
    750 - 1000   purple        (167, 67, 215) - extremely tall mountains
    >1000       dark purple   (111,49,140) - mountain equivalent of sky-scrapers

    0 - -300      white       (227,253,253)
    -300 - -800   white-blue  (188,223,241)
    -800 - -1500  light blue  (154,209,237)
    -1500 - -2500 blue        (130,195,229)
    -2500 - -3500 aqua        (90,167,207)
    -3500 - -5000 dark aqua   (47,129,172)
    <-5000        deep blue   (8,78,114)
    """

    if mean_elevation is None: return 0, 0, 0
    # elif mean_elevation < -1000: return 0, 60, 100
    # elif -1000 <= mean_elevation < -750: return 40, 110, 160
    # elif -750 <= mean_elevation < -500: return 90, 150, 190
    # elif -500 <= mean_elevation < -250: return 130, 180, 210
    # elif -250 <= mean_elevation < -100: return 150, 190, 220
    # elif -100 <= mean_elevation < -50: return 180, 210, 230
    # elif -50 <= mean_elevation < -10: return 190, 240, 240
    elif mean_elevation < 0: return 190, 240, 240
    elif mean_elevation == 0: return 230, 255, 255

    # elif 0 < mean_elevation < 10: return 124, 219, 135
    # elif 10 <= mean_elevation < 50: return 226, 234, 219
    # elif 50 <= mean_elevation < 100: return 210, 210, 189
    # elif 100 <= mean_elevation < 250: return 160, 162, 146
    # elif 250 <= mean_elevation < 500: return 95, 95, 95
    # elif 500 <= mean_elevation < 750: return 46, 46, 46
    # elif 750 <= mean_elevation < 1000: return 167, 67, 215
    # elif mean_elevation >= 1000: return 111, 49, 140

    elif 0 < mean_elevation < 10: return 124, 219, 135
    elif 10 <= mean_elevation < 50: return 175, 226, 177

    elif 50 <= mean_elevation < 100: return 226, 234, 219
    elif 100 <= mean_elevation < 150: return 218, 222, 204

    elif 150 <= mean_elevation < 200: return 210, 210, 189
    elif 200 <= mean_elevation < 250: return 185, 186, 168

    elif 250 <= mean_elevation < 300: return 160, 162, 146
    elif 300 <= mean_elevation < 350: return 138, 140, 129
    elif 350 <= mean_elevation < 400: return 116, 118, 112

    elif 400 <= mean_elevation < 450: return 95, 95, 95
    elif 450 <= mean_elevation < 500: return 71, 71, 71

    elif 500 <= mean_elevation < 550: return 46, 46, 46
    elif 550 <= mean_elevation < 600: return 70, 50, 80
    elif 600 <= mean_elevation < 650: return 94, 54, 114
    elif 650 <= mean_elevation < 700: return 118, 58, 148
    elif 700 <= mean_elevation < 750: return 142, 62, 182

    elif 750 <= mean_elevation < 800: return 167, 67, 215
    elif 800 <= mean_elevation < 850: return 156, 63, 200
    elif 850 <= mean_elevation < 900: return 145, 59, 185
    elif 900 <= mean_elevation < 950: return 134, 55, 170
    elif 950 <= mean_elevation < 1000: return 123, 51, 155
    elif mean_elevation >= 1000: return 111, 49, 140

    else: return 0, 0, 0


def ruggedness_to_rgb(elevations):
    rgb_tuple_list = []
    for elevation in elevations:
        rgb_tuple_list.append(ruggedpoint_to_rgb(elevation))
    return rgb_tuple_list


def elevations_asc_to_rugedness_image(cvs_path, image_name):
    file = open(cvs_path, "r")

    print("\n Reading file for rugedness image")
    elevations_matrix = []
    i=0
    for line in file:
        i += 1
        if i % 100 == 0:
            print("loaded line ", i)
        elevations_line = parse_line(line)
        if not elevations_line:
            print("Jumped over line: '" + line + "'")
            continue
        elevations_matrix.append(elevations_line)

    print("converting elevation to rugedness")
    rugedness_matrix = deepcopy(elevations_matrix)
    for r in range(0, len(elevations_matrix)):
        if r % 100 == 0:
            print("Row " + str(r) + "/" + str(len(elevations_matrix)))
        for c in range(0, len(elevations_matrix[0])):
            rugedness_matrix[r][c] = mean_elevation(r, c, elevations_matrix)

    rgb_tuple = []

    l = len(rugedness_matrix)
    i = 0
    for line in rugedness_matrix:
        if i % 100 == 0:
            print("Computing rugedness, line " + str(i) + "/" + str(l))
        rgb_tuple += ruggedness_to_rgb(line)
        i += 1

    img = Image.new('RGB', (len(elevations_matrix[0]), len(elevations_matrix)))
    img.putdata(rgb_tuple)
    img.save(image_name + '.png')
    print("Saved rugedness image!")


def mean_elevation(row, col, matrix):
    sum = 0
    n = 0
    for i in range(max(0, row - 1), min(len(matrix), row + 2)):
        for j in range(max(0, col - 1), min(len(matrix[0]), col + 2)):
            if i == row and j == col: continue
            sum += abs(matrix[row][col] - matrix[i][j])
            n += 1
    # print(row, col)
    rez = sum / n
    if matrix[row][col] <= 0:
        rez *= -1
        # rez -= 0.01

    return rez
