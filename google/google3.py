from copy import deepcopy
from typing import List

from PIL import Image
from numpy.core.function_base import linspace
import googlemaps
import numpy as np


def get_locations_list(start_latitude:float, start_longitude:float, nb_of_points) -> List[tuple]:
	list = []
	current_longitude = start_longitude
	for i in range (0, nb_of_points):
		list.append((round(start_latitude, 2), round(current_longitude, 2)))
		current_longitude += 0.02
	
	return list

def get_500Points_line_elevations(line: List) -> List:
	gmaps = googlemaps.Client(key='AIzaSyC0j9OEnXQ99rqEjgiVvrpFhMX5KQ5iFYk')
	results = gmaps.elevation(line)
	
	elevations_list = []
	for result in results:
		elevations_list.append((round(result['location']['lat'], 2), round(result['location']['lng'], 2),
		                        round(result['elevation'],2)))
	elevations_list.sort(key=lambda x: x[1])
	return elevations_list

def get_points_elevations_matrix(start_latitude: float, start_longitude: float, nb_of_500Points_lines, height) -> List[List[tuple]]:
	matrix = []
	
	for j in range (0, height):
		complete_line = []
		for i in range(0, nb_of_500Points_lines):
			print("Performing request " + str(j*nb_of_500Points_lines + i + 1) + "/" + str(height*nb_of_500Points_lines))
			locations_line = get_locations_list(start_latitude - j*0.02, start_longitude + i*10, 500)
			complete_line += get_500Points_line_elevations(locations_line)
		matrix.append(complete_line)
		
	return matrix

def append_matrix_to_cvs(cvs_path, matrix:List[List[tuple]]):
	file = open(cvs_path, "a+")
	for row in matrix:
		file.write(elevations_line_to_str(row))
		file.write("\n")

def elevations_line_to_str(line:List[tuple]) -> str:
	rez = ""
	
	for tup in line:
		rez += str(tup) + " "
	
	return rez
			
def append_elevations_to_cvs(cvs_path, start_latitude: float, start_longitude: float, nb_of_500Points_lines, height):
	matrix = get_points_elevations_matrix(start_latitude, start_longitude, nb_of_500Points_lines, height)
	append_matrix_to_cvs(cvs_path, matrix)
		
		

def elevations_cvs_to_relief_image(cvs_path, image_name):
	file = open(cvs_path, "r")
	print("\n Reading file for relief image")
	elevations_matrix = []
	for line in file:
		digital_line = parse_line(line)
		elevations_line = [x[2] for x in digital_line]
		elevations_matrix.append(elevations_line)
	
	rgb_tuple = []
	
	l = len(elevations_matrix)
	i = 0
	for line in elevations_matrix:
		if i % 10 == 0: 
			print("computing relief line" + str(i) + "/" + str(l)) 
		rgb_tuple += elevations_to_rgb(line)
		i += 1
	
	img = Image.new('RGB', (len(elevations_matrix[1]), len(elevations_matrix)))
	img.putdata(rgb_tuple)
	img.save(image_name + '.png')

def parse_line(line_string:str) -> List[tuple]:
	line_string = line_string.strip()
	elements = line_string.split(") (")
	digital_line = []
	for element in elements:
		tup = []
		element = element.strip()
		element = element.strip(")")
		element = element.strip("(")
		numbers = element.split(",")
		for number in numbers:
			number = number.strip()
			tup.append(float(number))
		digital_line.append(tuple(tup))
	
	return digital_line


def elevations_to_rgb(elevations):
	rgb_tuple_list = []
	for elevation in elevations:
		rgb_tuple_list.append(elevation_to_rgb(elevation))
	return rgb_tuple_list


def elevation_to_rgb(elevation: float):
	"""
	elevation   color          rgb
	0 - 300     green         (151,210,140)
	300 - 800   yellow        (241,233,172)
	800 - 1500  yellow-brown  (219,207,167)
	1500 - 2500 light brown   (184,166,124)
	2500 - 3500 brown         (148,130,93)
	3500 - 5000 dark brown    (102,89,64)
	>5000       grey          (201,198,193)

	0 - -300      white       (227,253,253)
	-300 - -800   white-blue  (188,223,241)
	-800 - -1500  light blue  (154,209,237)
	-1500 - -2500 blue        (130,195,229)
	-2500 - -3500 aqua        (90,167,207)
	-3500 - -5000 dark aqua   (47,129,172)
	<-5000        deep blue   (8,78,114)
	"""

	if elevation is None: return 0, 0, 0
	elif elevation < -5000: return 8, 78, 114
	elif -5000 <= elevation < -3500: return 47, 129, 172
	elif -3500 <= elevation < -2500: return 90, 167, 207
	elif -2500 <= elevation < -1500: return 130, 195, 229
	elif -1500 <= elevation < -800: return 154, 209, 237
	elif -800 <= elevation < -300: return 188, 223, 241
	elif -300 <= elevation <= 0: return 227, 253, 253

	elif 0 < elevation < 300: return 151, 210, 140
	elif 300 <= elevation < 800: return 241, 233, 172
	elif 800 <= elevation < 1500: return 219, 207, 167
	elif 1500 <= elevation < 2500: return 184, 166, 124
	elif 2500 <= elevation < 3500: return 148, 130, 93
	elif 3500 <= elevation < 5000: return 102, 89, 64
	elif elevation > 5000: return 201, 198, 193
	
	# elif 0 < elevation < 125: return 151, 210, 140
	# elif 125 <= elevation < 250: return 196, 222, 156
	# 
	# elif 250 <= elevation < 500: return 241, 233, 172
	# elif 500 <= elevation < 750: return 234, 224, 170
	# elif 750 <= elevation < 1000: return 227, 215, 168
	# 
	# elif 1000 <= elevation < 1250: return 219, 207, 167
	# elif 1250 <= elevation < 1500: return 207, 193, 153
	# elif 1500 <= elevation < 1750: return 195, 179, 139
	# 
	# elif 1750 <= elevation < 2000: return 184, 166, 124
	# elif 2000 <= elevation < 2250: return 172, 154, 114
	# elif 2250 <= elevation < 3000: return 160, 142, 104
	# 
	# elif 3000 <= elevation < 3250: return 148, 130, 93
	# elif 3250 <= elevation < 3500: return 133, 117, 83
	# elif 3500 <= elevation < 3750: return 118, 104, 73
	# 
	# elif 3750 <= elevation < 4000: return 102, 89, 64
	# elif 4000 <= elevation < 4250: return 122, 111, 90
	# elif 4250 <= elevation < 4500: return 142, 133, 116
	# elif 4500 <= elevation < 4750: return 162, 155, 142
	# elif 4750 <= elevation < 5000: return 182, 177, 168
	# 
	# elif elevation > 5000: return 201, 198, 193
	else:
		print(elevation)
		return 0, 0, 0


def ruggedpoint_to_rgb(mean_elevation: float):
	"""
	rugedness   color          rgb
	0 - 10      green         (124,219,135) - basically flat
	10 - 50     white-gray    (226,234,219) - a bit hilly
	50 - 100    light-gray    (210, 210, 189) - tall hills
	100 - 250   gray          (160, 162, 146) - tame mountains
	250 - 500   dark gray     (95, 95, 95) - mountains
	500 - 750   black         (46, 46, 46) - tall mountains
	750 - 1000   purple        (167, 67, 215) - extremely tall mountains
	>1000       dark purple   (111,49,140) - mountain equivalent of sky-scrapers

	0 - -300      white       (227,253,253)
	-300 - -800   white-blue  (188,223,241)
	-800 - -1500  light blue  (154,209,237)
	-1500 - -2500 blue        (130,195,229)
	-2500 - -3500 aqua        (90,167,207)
	-3500 - -5000 dark aqua   (47,129,172)
	<-5000        deep blue   (8,78,114)
	"""

	if mean_elevation is None: return 0, 0, 0
	elif mean_elevation < -1000: return 0, 60, 100
	elif -1000 <= mean_elevation < -750: return 40, 110, 160
	elif -750 <= mean_elevation < -500: return 90, 150, 190
	elif -500 <= mean_elevation < -250: return 130, 180, 210
	elif -250 <= mean_elevation < -100: return 150, 190, 220
	elif -100 <= mean_elevation < -50: return 180, 210, 230
	elif -50 <= mean_elevation < -10: return 190, 240, 240
	elif -10 <= mean_elevation < 0: return 230, 255, 255

	elif 0 <= mean_elevation < 10: return 124, 219, 135
	elif 10 <= mean_elevation < 50: return 226, 234, 219
	elif 50 <= mean_elevation < 100: return 210, 210, 189
	elif 100 <= mean_elevation < 250: return 160, 162, 146
	elif 250 <= mean_elevation < 500: return 95, 95, 95
	elif 500 <= mean_elevation < 750: return 46, 46, 46
	elif 750 <= mean_elevation < 1000: return 167, 67, 215
	elif mean_elevation >= 1000: return 111, 49, 140
	
	# elif 0 < mean_elevation < 10: return 124, 219, 135
	# elif 10 <= mean_elevation < 50: return 175, 226, 177
	# 
	# elif 50 <= mean_elevation < 100: return 226, 234, 219
	# elif 100 <= mean_elevation < 150: return 218, 222, 204
	# 
	# elif 150 <= mean_elevation < 200: return 210, 210, 189
	# elif 200 <= mean_elevation < 250: return 185, 186, 168
	# 
	# elif 250 <= mean_elevation < 300: return 160, 162, 146
	# elif 300 <= mean_elevation < 350: return 138, 140, 129
	# elif 350 <= mean_elevation < 400: return 116, 118, 112
	# 
	# elif 400 <= mean_elevation < 450: return 95, 95, 95
	# elif 450 <= mean_elevation < 500: return 71, 71, 71
	# 
	# elif 500 <= mean_elevation < 550: return 46, 46, 46
	# elif 550 <= mean_elevation < 600: return 70, 50, 80
	# elif 600 <= mean_elevation < 650: return 94, 54, 114
	# elif 650 <= mean_elevation < 700: return 118, 58, 148
	# elif 700 <= mean_elevation < 750: return 142, 62, 182
	# 
	# elif 750 <= mean_elevation < 800: return 167, 67, 215
	# elif 800 <= mean_elevation < 850: return 156, 63, 200
	# elif 850 <= mean_elevation < 900: return 145, 59, 185
	# elif 900 <= mean_elevation < 950: return 134, 55, 170
	# elif 950 <= mean_elevation < 1000: return 123, 51, 155
	# elif mean_elevation >= 1000: return 111, 49, 140
	
	else: return 0, 0, 0

def ruggedness_to_rgb(elevations):
	rgb_tuple_list = []
	for elevation in elevations:
		rgb_tuple_list.append(ruggedpoint_to_rgb(elevation))
	return rgb_tuple_list

def elevations_cvs_to_rugedness_image(cvs_path, image_name):
	file = open(cvs_path, "r")

	print("\n Reading file for rugedness image")
	elevations_matrix = []
	for line in file:
		digital_line = parse_line(line)
		elevations_line = [x[2] for x in digital_line]
		elevations_matrix.append(elevations_line)
	
	print("converting elevation to rugedness")
	rugedness_matrix = deepcopy(elevations_matrix)
	for r in range(0, len(elevations_matrix)):
		if r % 10 == 0:
			print("Row " + str(r) + "/" + str(len(elevations_matrix)))
		for c in range(0, len(elevations_matrix[0])):
			rugedness_matrix[r][c] = mean_elevation(r, c, elevations_matrix)

	rgb_tuple = []

	l = len(rugedness_matrix)
	i = 0
	for line in rugedness_matrix:
		if i % 10 == 0:
			print("Computing rugedness, line " + str(i) + "/" + str(l))
		rgb_tuple += ruggedness_to_rgb(line)
		i += 1

	img = Image.new('RGB', (len(elevations_matrix[1]), len(elevations_matrix)))
	img.putdata(rgb_tuple)
	img.save(image_name + '.png')


def mean_elevation(row, col, matrix):
	sum = 0
	n = 0
	for i in range(max(0, row-1), min(len(matrix), row+2)):
		for j in range (max(0, col-1), min(len(matrix[0]), col+2)):
			if i == j: continue
			sum += abs(matrix[row][col] - matrix[i][j])
			n += 1
	# print(row, col)
	rez = sum/n
	if matrix[row][col] <= 0:
		rez *= -1
		rez -= 0.01
		
	return rez

# path = "C:/Users/Andrei Mihai/Desktop/andrei workplace/Python/elevation_map_project/world.cvs"

# google3.append_elevations_to_cvs(path, 71.30, -180.00, 36, 68) c
# google3.append_elevations_to_cvs(path, 69.94, -180.00, 36, 68) c
# google3.append_elevations_to_cvs(path, 68.58, -180.00, 36, 68) c
# google3.append_elevations_to_cvs(path, 67.22, -180.00, 36, 68) c # last one computed
# google3.append_elevations_to_cvs(path, 65.86, -180.00, 36, 68) c
# google3.append_elevations_to_cvs(path, 64.50, -180.00, 36, 68) c
# google3.append_elevations_to_cvs(path, 63.14, -180.00, 36, 68) c
# google3.append_elevations_to_cvs(path, 61.78, -180.00, 36, 68) c
# google3.append_elevations_to_cvs(path, 60.42, -180.00, 36, 68) c
# google3.append_elevations_to_cvs(path, 59.06, -180.00, 36, 68) c
# google3.append_elevations_to_cvs(path, 57.70, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 56.34, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 54.98, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 53.62, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 52.26, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 50.90, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 49.54, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 48.18, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 46.82, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 45.46, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 44.10, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 42.74, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 41.38, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 40.02, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 38.66, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 37.30, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 35.94, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 33.22, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 31.86, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 30.50, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 29.14, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 27.78, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 26.42, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 25.06, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 23.70, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 22.34, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 20.98, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 19.62, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 18.26, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 16.90, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 15.54, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 14.18, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 12.82, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 11.46, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 10.10, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 8.74, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 7.38, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 6.02, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 4.66, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 3.30, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 1.94, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, 0.58, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -0.78, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -2.14, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -3.50, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -4.86, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -6.22, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -7.58, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -8.94, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -10.30, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -11.16, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -13.20, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -14.38, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -15.74, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -17.10, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -18.46, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -19.82, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -21.18, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -22.54, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -23.90, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -25.26, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -26.62, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -27.98, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -29.34, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -30.70, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -32.06, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -33.42, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -34.78, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -36.14, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -37.50, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -38.86, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -40.22, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -41.58, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -42.94, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -44.30, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -45.66, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -47.02, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -48.38, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -49.74, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -51.10, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -52.46, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -53.82, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -55.18, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -56.54, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -57.90, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -59.26, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -60.62, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -61.98, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -63.34, -180.00, 36, 68)
# google3.append_elevations_to_cvs(path, -64.70, -180.00, 36, 68)

# google3.elevations_cvs_to_relief_image(path, "World_relief")
# google3.elevations_cvs_to_rugedness_image(path, "World_rugedness")
