from copy import deepcopy
from typing import List
from PIL import Image
# from urllib.request import urlopen
import json

# from bingmaps.apiservices import ElevationsApi


# def line_to_str_copied(line: List[tuple]) -> str:
# 	latitude = 0
# 	longitude = 0
# 	result = ""
# 	# l
#
# 	for point in line:
# 		# step 2
# 		new_latitude = round(point[0] * 100000)
# 		new_longitude = round(point[1] * 100000)
#
# 		# // step 3
# 		dy = new_latitude - latitude
# 		dx = new_longitude - longitude
# 		latitude = new_latitude
# 		longitude = new_longitude
#
# 		# // step 4 and 5
# 		dy = (dy << 1) ^ (dy >> 31)
# 		dx = (dx << 1) ^ (dx >> 31)
#
# 		# // step 6
# 		index = ((dy + dx) * (dy + dx + 1) / 2) + dy
#
# 		while index > 0:
# 			# // step 7
# 			# rem = index & 31
# 			# index = (index - rem) / 32
#
# 			(index, rem) = divmod(index, 32)
# 			rem = int(rem)
#
# 			# // step 8
# 			if (index > 0): rem += 32
#
# 			# // step 9
# 			result += "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-"[rem]
#
# 	# // step 10
# 	return result
#
# def niceprint(line:List, step:str):
# 	print("\n " + step)
# 	for l in line:
# 		rez = ""
# 		if type(l) == list:
# 			for i in range(0, len(l)):
# 				rez += str(l[i]) + ", "
# 		else:
# 			rez = str(l)
# 		print(rez)
#
# def line_to_str(line: List[tuple]) -> str:
#
# 	# Step 2
# 	multiplied = []
# 	for pair in line:
# 		lat = pair[0]
# 		long = pair[1]
#
# 		lat *= 100_000
# 		lat = round(lat)
#
# 		long *= 100_000
# 		long = round(long)
# 		multiplied.append((lat, long))
# 	# niceprint(multiplied, "Step 2: multiplied")
#
# 	# Step 3
# 	differences = []
# 	last_lat = 0
# 	last_long = 0
# 	for pair in multiplied:
# 		lat = pair[0]
# 		long = pair[1]
#
#
# 		#Step 3a
# 		new_lat = lat - last_lat
# 		new_long = long - last_long
#
# 		#Step 3b
# 		if new_long > 18_000_000: new_long -= 36_000_000
# 		if new_long < -18_000_000: new_long += 36_000_000
#
# 		differences.append((new_lat, new_long))
#
# 		last_lat = lat
# 		last_long = long
# 	# niceprint(differences, "Step 3: differences")
#
# 	# Step 4
# 	doubled = []
# 	for pair in differences:
# 		lat = pair[0]
# 		long = pair[1]
#
# 		new_lat = lat * 2
# 		new_long = long * 2
# 		doubled.append((new_lat, new_long))
# 	# niceprint(doubled, "Step 4: doubled")
#
# 	# Step 5
# 	pozitivied = []
# 	for pair in doubled:
# 		lat = pair[0]
# 		long = pair[1]
#
# 		if lat < 0: lat = abs(lat) - 1
# 		if long < 0: long = abs(long) - 1
# 		pozitivied.append((lat, long))
# 	# niceprint(pozitivied, "Step 5: pozitivied")
#
# 	# Step 6
# 	combined = []
# 	for pair in pozitivied:
# 		lat = pair[0]
# 		long = pair[1]
#
# 		combination = ((lat + long) * (lat + long + 1) / 2) + lat
# 		combined.append(combination)
# 	# niceprint(combined, "Step 6: combined")
#
# 	# Step 7
# 	numberified = []
# 	for combination in combined:
# 		quotient = combination
# 		(quotient, reminder) = divmod(quotient, 32)
# 		numbers = [reminder]
#
# 		while quotient > 0:
# 			(quotient, reminder) = divmod(quotient, 32)
# 			numbers.append(reminder)
# 		numberified.append(numbers)
# 	# niceprint(numberified, "Step 7: divided to 32")
#
# 	# Step 8
# 	added = []
# 	for numbers in numberified:
# 		r = []
# 		for i in range(0, len(numbers) - 1):
# 			r.append(numbers[i] + 32)
# 		r.append(numbers[-1])
# 		added.append(r)
# 	# niceprint(added, "Step 8: added 32")
#
# 	# Step 9 and 10
# 	rez = ""
# 	for numbers in added:
# 		for number in numbers:
# 			rez += "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-"[int(number)]
#
# 	return rez
#
# def get_1000Points_line_elevations(line: List[tuple]) -> List:
# 	key = "Alb8vIdEBmtNDTrVswJHVv6O_ThDQ8S_jRVIoIvctt2jFp5uYpLvn46izBJWVs2p"
# 	# data = {'method':'List', 'points':line, 'key':key}
# 	# n = len(line)
#
# 	points = line_to_str(line)
#
# 	# data = {'method':'List', 'points':points, 'key':key}
#
# 	# results = ElevationsApi(data)
#
# 	r = "http://dev.virtualearth.net/REST/v1/Elevation/List?points=" + points + "&heights=ellipsoid" \
# 	                                                                            "&key=" + key
# 	# o = "http://dev.virtualearth.net/REST/v1/Elevation/SeaLevel?points=" + points + "" \
# 	#                                                                             "&key=" + key
# 	content = urlopen(r).read()
# 	results = json.loads(content.decode("utf-8"))
#
# 	# o_content = urlopen(o).read()
# 	# o_results = json.loads(o_content.decode("utf-8"))
#
# 	elevations_list = []
# 	i = 0
# 	# for elevation in results.elevations[0].elevations:
# 	# 	elevations_list.append((round(line[i], 2), round(line[i+1], 2),
# 	# 	                        round(elevation, 2)))
# 	# 	i += 2
# 	#
# 	# offsets = o_results['resourceSets'][0]['resources'][0]['offsets']
# 	for elevation in results['resourceSets'][0]['resources'][0]['elevations']:
# 		elevations_list.append((round(line[i][0], 2), round(line[i][1], 2),
# 		                        round(elevation, 2)))
# 		i += 1
#
# 	# elevations_list.sort(key=lambda x:x[1])
# 	return elevations_list
#
#
# def get_locations_list(start_latitude: float, start_longitude: float, nb_of_points) -> List[tuple]:
# 	l = []
# 	current_longitude = start_longitude
# 	for i in range(0, nb_of_points):
# 		# l.append(round(start_latitude, 2))
# 		# l.append(round(current_longitude, 2))
#
# 		l.append(((round(start_latitude, 2)), round(current_longitude, 2)))
#
# 		current_longitude += 0.02
#
# 	return l
#
#
# def get_points_elevations_matrix(start_latitude: float, start_longitude: float, nb_of_1000_points_lines, height) -> List[
# 	List[tuple]]:
# 	matrix = []
#
# 	for j in range(0, height):
# 		complete_line = []
# 		for i in range(0, nb_of_1000_points_lines):
# 			print("Performing request " + str(j * nb_of_1000_points_lines + i + 1) + "/" + str(
# 					height * nb_of_1000_points_lines))
# 			locations_line = get_locations_list(start_latitude - j * 0.02, start_longitude + i * 7.5, 375)
# 			complete_line += get_1000Points_line_elevations(locations_line)
# 		matrix.append(complete_line)
#
# 	return matrix


# vvvvvvvvvvvvvvvvvvvvvvvvv  NOT BING RELATED vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv


def append_matrix_to_cvs(cvs_path, matrix: List[List[tuple]]):
	file = open(cvs_path, "a+")
	for row in matrix:
		file.write(elevations_line_to_str(row))
		file.write("\n")


def elevations_line_to_str(line: List[tuple]) -> str:
	rez = ""

	for tup in line:
		rez += str(tup) + " "

	return rez


# def append_elevations_to_cvs(cvs_path, start_latitude: float, start_longitude: float, nb_of_500Points_lines, height):
# 	print("Start Lattitude: ", start_latitude)
# 	matrix = get_points_elevations_matrix(start_latitude, start_longitude, nb_of_500Points_lines, height)
# 	append_matrix_to_cvs(cvs_path, matrix)


def elevations_cvs_to_relief_image(cvs_path, image_name):
	file = open(cvs_path, "r")
	print("\n Reading file for relief image")
	elevations_matrix = []
	for line in file:
		digital_line = parse_line(line)
		elevations_line = [x[2] for x in digital_line]
		elevations_matrix.append(elevations_line)

	rgb_tuple = []

	l = len(elevations_matrix)
	i = 0
	for line in elevations_matrix:
		if i % 10 == 0:
			print("computing relief line" + str(i) + "/" + str(l))
		rgb_tuple += elevations_to_rgb(line)
		i += 1

	img = Image.new('RGB', (len(elevations_matrix[0]), len(elevations_matrix)))
	img.putdata(rgb_tuple)
	img.save(image_name + '.png')


def parse_line(line_string: str) -> List[tuple]:
	line_string = line_string.strip()
	elements = line_string.split(") (")
	digital_line = []
	for element in elements:
		tup = []
		element = element.strip()
		element = element.strip(")")
		element = element.strip("(")
		numbers = element.split(",")
		for number in numbers:
			number = number.strip()
			tup.append(float(number))
		digital_line.append(tuple(tup))

	return digital_line


def elevations_to_rgb(elevations):
	rgb_tuple_list = []
	for elevation in elevations:
		rgb_tuple_list.append(elevation_to_rgb(elevation))
	return rgb_tuple_list


def elevation_to_rgb(elevation: float):
	"""
	elevation   color          rgb
	0 - 300     green         (151,210,140)
	300 - 800   yellow        (241,233,172)
	800 - 1500  yellow-brown  (219,207,167)
	1500 - 2500 light brown   (184,166,124)
	2500 - 3500 brown         (148,130,93)
	3500 - 5000 dark brown    (102,89,64)
	>5000       grey          (201,198,193)

	0 - -300      white       (227,253,253)
	-300 - -800   white-blue  (188,223,241)
	-800 - -1500  light blue  (154,209,237)
	-1500 - -2500 blue        (130,195,229)
	-2500 - -3500 aqua        (90,167,207)
	-3500 - -5000 dark aqua   (47,129,172)
	<-5000        deep blue   (8,78,114)
	"""

	if elevation is None: return 0, 0, 0
	elif elevation < -5000: return 8, 78, 114
	elif -5000 <= elevation < -3500: return 47, 129, 172
	elif -3500 <= elevation < -2500: return 90, 167, 207
	elif -2500 <= elevation < -1500: return 130, 195, 229
	elif -1500 <= elevation < -800: return 154, 209, 237
	elif -800 <= elevation < -300: return 188, 223, 241
	elif -300 <= elevation <= 0: return 227, 253, 253

	elif 0 < elevation < 300: return 151, 210, 140
	elif 300 <= elevation < 800: return 241, 233, 172
	elif 800 <= elevation < 1500: return 219, 207, 167
	elif 1500 <= elevation < 2500: return 184, 166, 124
	elif 2500 <= elevation < 3500: return 148, 130, 93
	elif 3500 <= elevation < 5000: return 102, 89, 64
	elif elevation > 5000: return 201, 198, 193

	# elif 0 < elevation < 125: return 151, 210, 140
	# elif 125 <= elevation < 250: return 196, 222, 156
	# 
	# elif 250 <= elevation < 500: return 241, 233, 172
	# elif 500 <= elevation < 750: return 234, 224, 170
	# elif 750 <= elevation < 1000: return 227, 215, 168
	# 
	# elif 1000 <= elevation < 1250: return 219, 207, 167
	# elif 1250 <= elevation < 1500: return 207, 193, 153
	# elif 1500 <= elevation < 1750: return 195, 179, 139
	# 
	# elif 1750 <= elevation < 2000: return 184, 166, 124
	# elif 2000 <= elevation < 2250: return 172, 154, 114
	# elif 2250 <= elevation < 3000: return 160, 142, 104
	# 
	# elif 3000 <= elevation < 3250: return 148, 130, 93
	# elif 3250 <= elevation < 3500: return 133, 117, 83
	# elif 3500 <= elevation < 3750: return 118, 104, 73
	# 
	# elif 3750 <= elevation < 4000: return 102, 89, 64
	# elif 4000 <= elevation < 4250: return 122, 111, 90
	# elif 4250 <= elevation < 4500: return 142, 133, 116
	# elif 4500 <= elevation < 4750: return 162, 155, 142
	# elif 4750 <= elevation < 5000: return 182, 177, 168
	# 
	# elif elevation > 5000: return 201, 198, 193
	else:
		print(elevation)
		return 0, 0, 0


def ruggedpoint_to_rgb(mean_elevation: float):
	"""
	rugedness   color          rgb
	0 - 10      green         (124,219,135) - basically flat
	10 - 50     white-gray    (226,234,219) - a bit hilly
	50 - 100    light-gray    (210, 210, 189) - tall hills
	100 - 250   gray          (160, 162, 146) - tame mountains
	250 - 500   dark gray     (95, 95, 95) - mountains
	500 - 750   black         (46, 46, 46) - tall mountains
	750 - 1000   purple        (167, 67, 215) - extremely tall mountains
	>1000       dark purple   (111,49,140) - mountain equivalent of sky-scrapers

	0 - -300      white       (227,253,253)
	-300 - -800   white-blue  (188,223,241)
	-800 - -1500  light blue  (154,209,237)
	-1500 - -2500 blue        (130,195,229)
	-2500 - -3500 aqua        (90,167,207)
	-3500 - -5000 dark aqua   (47,129,172)
	<-5000        deep blue   (8,78,114)
	"""

	if mean_elevation is None: return 0, 0, 0
	elif mean_elevation < -1000: return 0, 60, 100
	elif -1000 <= mean_elevation < -750: return 40, 110, 160
	elif -750 <= mean_elevation < -500: return 90, 150, 190
	elif -500 <= mean_elevation < -250: return 130, 180, 210
	elif -250 <= mean_elevation < -100: return 150, 190, 220
	elif -100 <= mean_elevation < -50: return 180, 210, 230
	elif -50 <= mean_elevation < -10: return 190, 240, 240
	elif -10 <= mean_elevation <= 0: return 230, 255, 255

	elif 0 < mean_elevation < 10: return 124, 219, 135
	elif 10 <= mean_elevation < 50: return 226, 234, 219
	elif 50 <= mean_elevation < 100: return 210, 210, 189
	elif 100 <= mean_elevation < 250: return 160, 162, 146
	elif 250 <= mean_elevation < 500: return 95, 95, 95
	elif 500 <= mean_elevation < 750: return 46, 46, 46
	elif 750 <= mean_elevation < 1000: return 167, 67, 215
	elif mean_elevation >= 1000: return 111, 49, 140

	# elif 0 < mean_elevation < 10: return 124, 219, 135
	# elif 10 <= mean_elevation < 50: return 175, 226, 177
	# 
	# elif 50 <= mean_elevation < 100: return 226, 234, 219
	# elif 100 <= mean_elevation < 150: return 218, 222, 204
	# 
	# elif 150 <= mean_elevation < 200: return 210, 210, 189
	# elif 200 <= mean_elevation < 250: return 185, 186, 168
	# 
	# elif 250 <= mean_elevation < 300: return 160, 162, 146
	# elif 300 <= mean_elevation < 350: return 138, 140, 129
	# elif 350 <= mean_elevation < 400: return 116, 118, 112
	# 
	# elif 400 <= mean_elevation < 450: return 95, 95, 95
	# elif 450 <= mean_elevation < 500: return 71, 71, 71
	# 
	# elif 500 <= mean_elevation < 550: return 46, 46, 46
	# elif 550 <= mean_elevation < 600: return 70, 50, 80
	# elif 600 <= mean_elevation < 650: return 94, 54, 114
	# elif 650 <= mean_elevation < 700: return 118, 58, 148
	# elif 700 <= mean_elevation < 750: return 142, 62, 182
	# 
	# elif 750 <= mean_elevation < 800: return 167, 67, 215
	# elif 800 <= mean_elevation < 850: return 156, 63, 200
	# elif 850 <= mean_elevation < 900: return 145, 59, 185
	# elif 900 <= mean_elevation < 950: return 134, 55, 170
	# elif 950 <= mean_elevation < 1000: return 123, 51, 155
	# elif mean_elevation >= 1000: return 111, 49, 140

	else: return 0, 0, 0


def ruggedness_to_rgb(elevations):
	rgb_tuple_list = []
	for elevation in elevations:
		rgb_tuple_list.append(ruggedpoint_to_rgb(elevation))
	return rgb_tuple_list


def elevations_cvs_to_rugedness_image(cvs_path, image_name):
	file = open(cvs_path, "r")

	print("\n Reading file for rugedness image")
	elevations_matrix = []
	for line in file:
		digital_line = parse_line(line)
		elevations_line = [x[2] for x in digital_line]
		elevations_matrix.append(elevations_line)

	print("converting elevation to rugedness")
	rugedness_matrix = deepcopy(elevations_matrix)
	for r in range(0, len(elevations_matrix)):
		if r % 10 == 0:
			print("Row " + str(r) + "/" + str(len(elevations_matrix)))
		for c in range(0, len(elevations_matrix[0])):
			rugedness_matrix[r][c] = mean_elevation(r, c, elevations_matrix)

	rgb_tuple = []

	l = len(rugedness_matrix)
	i = 0
	for line in rugedness_matrix:
		if i % 10 == 0:
			print("Computing rugedness, line " + str(i) + "/" + str(l))
		rgb_tuple += ruggedness_to_rgb(line)
		i += 1

	img = Image.new('RGB', (len(elevations_matrix[0]), len(elevations_matrix)))
	img.putdata(rgb_tuple)
	img.save(image_name + '.png')


def mean_elevation(row, col, matrix):
	sum = 0
	n = 0
	for i in range(max(0, row - 1), min(len(matrix), row + 2)):
		for j in range(max(0, col - 1), min(len(matrix[0]), col + 2)):
			if i == j: continue
			sum += abs(matrix[row][col] - matrix[i][j])
			n += 1
	# print(row, col)
	rez = sum / n
	if matrix[row][col] <= 0:
		rez *= -1
		rez -= 0.01

	return rez
