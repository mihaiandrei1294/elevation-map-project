import json
from typing import List
from urllib.request import urlopen

from PIL import Image
from numpy.core.function_base import linspace
import googlemaps
import numpy as np


def elevation_ruggedness_for_20Points_line(start_latitude: float, start_longitude: float, l=10):
	longitude_modifiers = linspace(0, 1.9, 20)

	points_subpoints_list = []
	total_locations_list = []

	for m in longitude_modifiers:
		first_latitude = start_latitude
		first_longitude = start_longitude + m

		equidistant_distances = linspace(0, l / 100, 5)
		equidistant_latitudes = equidistant_distances * (-1) + first_latitude
		equidistant_longitudes = equidistant_distances + first_longitude

		sub_points = []
		for latitude in equidistant_latitudes:
			for longitude in equidistant_longitudes:
				sub_points.append((round(latitude, 2), round(longitude, 2)))

		points_subpoints_list.append(sub_points)
		total_locations_list = total_locations_list + sub_points

	gmaps = googlemaps.Client(key='AIzaSyC0j9OEnXQ99rqEjgiVvrpFhMX5KQ5iFYk')
	result = gmaps.elevation(total_locations_list)

	points_rugedness = []
	for point in points_subpoints_list:
		elevation_sum = 0
		i = 0
		elevations_in_point = []
		for sub_point in point:
			i += 1
			elevation = find_subpoint_elevation(sub_point, result)
			if elevation is None:
				elevations_in_point.append(elevation_sum / i)
				elevation_sum += elevation_sum / i
			else:
				elevations_in_point.append(elevation)
				elevation_sum += elevation
		point_elevation = elevation_sum / len(point)
		if 0 > point_elevation:
			points_rugedness.append(point_elevation)
		else:
			points_rugedness.append(ruggednesss(elevations_in_point))

	points_elevations = []
	for point in points_subpoints_list:
		elevation_sum = 0
		foundNone = False
		i = 0
		for sub_point in point:
			i += 1
			elevation = find_subpoint_elevation(sub_point, result)
			if elevation is None:
				elevation_sum += elevation_sum / i
			else:
				elevation_sum += elevation

		if not foundNone:
			point_elevation = elevation_sum / len(point)
			points_elevations.append(point_elevation)

	return points_elevations, points_rugedness


def elevations_rugedness_for_mxn_points(start_latitude: float, start_longitude: float, number_of_20pLines,
                                        number_of_height_points, l=10):
	elevations = []
	ruggedness = []
	s = number_of_height_points * number_of_20pLines
	now = 0
	for h in range(0, number_of_height_points):
		for i in range(0, number_of_20pLines):
			now += 1
			print("Computing line " + str(now) + " of " + str(s))
			first_latitude = start_latitude - h / l
			first_longitude = start_longitude + i * 2
			e, r = elevation_ruggedness_for_20Points_line(first_latitude, first_longitude)
			ruggedness = ruggedness + r
			elevations = elevations + e

	return elevations, ruggedness

def create_both_images(name, start_latitude: float, start_longitude: float, number_of_20pLines, number_of_height_points,
                       l=10):
	elevations, rugged_points = elevations_rugedness_for_mxn_points(start_latitude, start_longitude, number_of_20pLines,
	                                          number_of_height_points, l)
	rugedness_rgb_tuple = ruggedness_to_rgb(rugged_points)
	rugedness_img = Image.new('RGB', (number_of_20pLines * 20, number_of_height_points))
	rugedness_img.putdata(rugedness_rgb_tuple)
	rugedness_img.save(name + ' mean elevation difference.png')

	rgb_tuple = elevations_to_rgb(elevations)
	img = Image.new('RGB', (number_of_20pLines * 20, number_of_height_points))
	img.putdata(rgb_tuple)
	img.save(name + '.png')
	

def ruggednesss(elevations: List):
	ne = np.array(elevations)

	mean = ne.mean()

	mean_differences = np.abs(ne - mean)
	return mean_differences.mean()


def ruggedpoint_to_rgb(elevation: float):
	"""
	rugedness   color          rgb
	0 - 10      green         (124,219,135) - basically flat
	10 - 50     white-gray    (226,234,219) - a bit hilly
	50 - 100    light-gray    (210, 210, 189) - tall hills
	100 - 250   gray          (160, 162, 146) - tame mountains
	250 - 500   dark gray     (95, 95, 95) - mountains
	500 - 750   black         (46, 46, 46) - tall mountains
	750 - 1000   purple        (167, 67, 215) - extremely tall mountains
	>1000       dark purple   (111,49,140) - mountain equivalent of sky-scrapers

	0 - -300      white       (227,253,253)
	-300 - -800   white-blue  (188,223,241)
	-800 - -1500  light blue  (154,209,237)
	-1500 - -2500 blue        (130,195,229)
	-2500 - -3500 aqua        (90,167,207)
	-3500 - -5000 dark aqua   (47,129,172)
	<-5000        deep blue   (8,78,114)
	"""

	if elevation is None: return 0, 0, 0
	elif elevation < -5000: return 8, 78, 114
	elif -5000 < elevation < -3500: return 47, 129, 172
	elif -3500 < elevation < -2500: return 90, 167, 207
	elif -2500 < elevation < -1500: return 130, 195, 229
	elif -1500 < elevation < -800: return 154, 209, 237
	elif -800 < elevation < -300: return 188, 223, 241
	elif -300 < elevation < 0: return 227, 253, 253

	elif 0 < elevation < 10: return 124, 219, 135
	elif 10 < elevation < 50: return 226, 234, 219
	elif 50 < elevation < 100: return 210, 210, 189
	elif 100 < elevation < 250: return 160, 162, 146
	elif 250 < elevation < 500: return 95, 95, 95
	elif 500 < elevation < 750: return 46, 46, 46
	elif 750 < elevation < 1000: return 167, 67, 215
	elif elevation > 1000: return 111, 49, 140
	else: return 0, 0, 0

def ruggedness_to_rgb(elevations):
	rgb_tuple_list = []
	for elevation in elevations:
		rgb_tuple_list.append(ruggedpoint_to_rgb(elevation))
	return rgb_tuple_list

def elevation_to_rgb(elevation: float):
	"""
	elevation   color          rgb
	0 - 300     green         (151,210,140)
	300 - 800   yellow        (241,233,172)
	800 - 1500  yellow-brown  (219,207,167)
	1500 - 2500 light brown   (184,166,124)
	2500 - 3500 brown         (148,130,93)
	3500 - 5000 dark brown    (102,89,64)
	>5000       grey          (201,198,193)

	0 - -300      white       (227,253,253)
	-300 - -800   white-blue  (188,223,241)
	-800 - -1500  light blue  (154,209,237)
	-1500 - -2500 blue        (130,195,229)
	-2500 - -3500 aqua        (90,167,207)
	-3500 - -5000 dark aqua   (47,129,172)
	<-5000        deep blue   (8,78,114)
	"""

	if elevation is None: return 0, 0, 0
	elif elevation < -5000: return 8, 78, 114
	elif -5000 < elevation < -3500: return 47, 129, 172
	elif -3500 < elevation < -2500: return 90, 167, 207
	elif -2500 < elevation < -1500: return 130, 195, 229
	elif -1500 < elevation < -800: return 154, 209, 237
	elif -800 < elevation < -300: return 188, 223, 241
	elif -300 < elevation < 0: return 227, 253, 253

	elif 0 < elevation < 300: return 151, 210, 140
	elif 300 < elevation < 800: return 241, 233, 172
	elif 800 < elevation < 1500: return 219, 207, 167
	elif 1500 < elevation < 2500: return 184, 166, 124
	elif 2500 < elevation < 3500: return 148, 130, 93
	elif 3500 < elevation < 5000: return 102, 89, 64
	elif elevation > 5000: return 201, 198, 193
	else: return 0, 0, 0

def elevations_to_rgb(elevations):
	rgb_tuple_list = []
	for elevation in elevations:
		rgb_tuple_list.append(elevation_to_rgb(elevation))
	return rgb_tuple_list

def find_subpoint_elevation(sub_point, results):
	for result in results:
		result_lat = round(result['location']['lat'], 2)
		result_lng = round(result['location']['lng'], 2)
		if result_lat == sub_point[0] and result_lng == sub_point[1]:
			return result['elevation']

	return None
