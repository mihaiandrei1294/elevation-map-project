import time
from PIL import Image

import asc_to_image
# from google import google, bing
# from google import google2
# from google import google3

# t0 = time.time()
# mean_elevation_targu_mures = google.mean_elevation_for_point(46.56,24.52)
# t1 = time.time()
#
#
# print(mean_elevation_targu_mures, t1-t0)

# nepal_himalaya_border = [[1737.8403271484376, 1419.7115649414063, 667.8210766601562, 423.0175988769531, 255.49215576171875], [1640.65873046875, 1511.4506787109376, 623.3136645507813, 457.54584838867186, 331.34443298339846]]

# 56.52, -3.71 - Edinburgh
# edinburgh = [[264.7454830932617, 82.70111328125, 231.59264091491698, 214.7474252319336, 41.38094254851341, 97.07184167861938, 189.3371646118164, 257.95283142089846, 224.3588525390625, 294.79935974121094], [122.4522216796875, 72.26621105194091, 253.5931901550293, 191.0821667480469, 71.36313468694686, 107.24808413863182, 184.98367950439453, 277.17427490234377, 240.8424444580078, 333.22176635742187], [58.970271072387696, 85.7060440826416, 207.40968048095704, 169.4873306274414, 93.22742942690849, 55.426732335090634, 198.38176666259767, 342.7891052246094, 270.0656805419922, 337.30287170410156], [112.18032028198242, 97.0694807434082, 172.34651916503907, 139.9352587890625, 87.61131274223328, 38.00596548676491, 232.75861694335939, 328.2920159912109, 293.03973571777345, 389.2430975341797], [136.01715755462646, 80.32844491958618, 194.39363494873047, 136.34698699951173, 68.96062828063965, 38.87041459083557, 225.0441519165039, 288.5966369628906, 292.4214147949219, 467.28289672851565], [94.90130080223084, 66.53867359876632, 96.08461975097656, 82.58876299023628, 16.7558547103405, 33.155801967978476, 148.7909700012207, 308.3017181396484, 330.24781616210936, 417.7946301269531], [67.96492431163787, 60.23021065711975, 91.43275360107423, 32.77619712114334, -16.46389016985893, 10.036719164848328, 126.8294400024414, 346.4157922363281, 399.9913537597656, 309.3232788085937], [66.47871599197387, 61.842825946807864, 104.79515686035157, -1.5718639886379249, -21.353319940567015, 33.08478666841984, 175.5487173461914, 330.68597717285155, 381.2920751953125, 301.69593322753906], [41.775532671809195, 35.058331917524335, 123.47285018920898, -7.616206376552582, -16.48703667908907, 53.33636160463094, 195.99181762695312, 318.56117553710936, 279.22853393554686, 243.22582794189452], [13.3546483117342, 11.128585149049758, 79.91556045532226, -20.8071560049057, -2.5155698972940455, 67.45784530639648, 261.5411059570313, 303.99260375976564, 230.59247192382813, 196.52169036865234]]\

# elevations = google.elevations_for_20Points_line(46.56,24.52)
# rgb_tuple = google.elevationMatrix_to_rgbTupleList([elevations])
# img = Image.new('RGB', (20, 1))
# img.putdata(rgb_tuple)
# img.save('linetest.png')

#--------- Lines & Points implementation -----------

# 35.8, 32.07 - Cyprus (1 line, 16 points)
# 41.33, 19.23 - Greece, Agean & part Turkey (4 lines, 63 points)
# 22.48, -160.35 - Hawaii (3 lines, 41 points)
# 59.52, -11.22 - England & Ireland (7 lines, 100 points)
# 45.55, 128.58 - Japan (9 lines, 153 points)
# 48.27, 20.25 - Romania (5 lines, 46 points)
# 36.51, 50.20 - Teheran (2 lines, 17 points)

# google.create_image('Romania',48.27,128.58,5,46)
# google.create_ruggedness_image('Japan', 45.55, 20.25, 9, 153)

# google2.create_both_images('Teheran',36.51,50.20,2,17)




##################################      EUROPE        #############################################

#Europe Extreme points:
# North Norway: 71.20 (25.65),        South: 34.42 (32.97), East Azerbaidjan: (40.31) 50.68, West Iceland: (65.51) -24.64
# North Iceland: 66.57 (-16.12)                             East Urals: (56.54) 60.89        West Ireland: (52.04) -10.64
# North Russia - Island: 76.11 (59.66)                      East Black Sea: (41.80) 41.79

# Biggest Europe: 42 lines 417 points (=> 17,514 requests, 8 days, 53 points/day)
# Smallest Europe: 26 lines 321 points (=> 8,346 requests, 4 days, 81 points/day)
# Preferred Europe (norway, azerbaidjan, iceland): 37 lines, 368 points (13,616 requests, 6 days, 62 points/day)

#Biggest Europe Implementation:
# google.create_image('Europe 1',76.11,-24.64,42,53)
# google.create_image('Europe 2',70.81,-24.64,42,53)
# google.create_image('Europe 3',65.51,-24.64,42,53)
# google.create_image('Europe 4',60.21,-24.64,42,53)
# google.create_image('Europe 5',54.91,-24.64,42,53)
# google.create_image('Europe 6',49.61,-24.64,42,53)
# google.create_image('Europe 7',44.31,-24.64,42,53)
# google.create_image('Europe 8',39.01,-24.64,42,53)

# google.create_ruggedness_image('Europe 1',76.11,-24.64,42,53)
# google.create_ruggedness_image('Europe 2',70.81,-24.64,42,53)
# google.create_ruggedness_image('Europe 3',65.51,-24.64,42,53)
# google.create_ruggedness_image('Europe 4',60.21,-24.64,42,53)
# google.create_ruggedness_image('Europe 5',54.91,-24.64,42,53)
# google.create_ruggedness_image('Europe 6',49.61,-24.64,42,53)
# google.create_ruggedness_image('Europe 7',44.31,-24.64,42,53)
# google.create_ruggedness_image('Europe 8',39.01,-24.64,42,53)

######################################################################################################


######################      ASIAN MOUNTAIN RANGES & PLATEAUS        ##################################

#Asian mountains extreme points:
#North Russia: 56.22 (92.35),    South Myanmar: 15.67 (95.03), West Turkmenistan: (37.79) 65.58, East China: (29.90) 122.48

#Asia mountains info: 29 lines, 406 points (=> 11,774 requests, 5 days,  82 points/day)

#Implementation:
# google.create_image('Asian Mountains 1',56.22,65.58,29,82)
# google.create_image('Asian Mountains 2',48.02,65.58,29,82)
# google.create_image('Asian Mountains 3',39.82,65.58,29,82)
# google.create_image('Asian Mountains 4',31.62,65.58,29,82)
# google.create_image('Asian Mountains 5',23.42,65.58,29,82)


# google.create_ruggedness_image('Asian Mountains 1', 56.22, 65.58, 29, 82)
# google.create_ruggedness_image('Asian Mountains 2',48.02,65.58,29,82)
# google.create_ruggedness_image('Asian Mountains 3',39.82,65.58,29,82)
# google.create_ruggedness_image('Asian Mountains 4',31.62,65.58,29,82)
# google.create_ruggedness_image('Asian Mountains 5',23.42,65.58,29,82)

######################################################################################################


##########################         UNITED STATES OF AMERICA            ###############################


# USA extreme points:
# North Minnesota: 49.38 (-95.15), South Florida: 24.54 (-81.80), West Washington: (48.18) -124.78, East Maine: (44.81)
#  -66.94

# USA info: 29 lines, 248 points (=> 7,192 requests, 3 days,  85 points/day)

# Implementation:
# google.create_image('USA 1', 49.38, -124.78,29,85)
# google.create_image('USA 2',40.88,-124.78,29,85)
# google.create_image('USA 3',32.38,-124.78,29,85)
# 
# 
# google.create_ruggedness_image('USA 1',49.38,-124.78,29,85)
# google.create_ruggedness_image('USA 2',40.88,-124.78,29,85)
# google.create_ruggedness_image('USA 3',32.38,-124.78,29,85)

######################################################################################################


################################           THE WORLD            ######################################

# World extreme points:
# North Russia: 70.00 (180.00), South Chile: -57.00 (-67.47), West Russia: (70.00) -180.00, East Russia: (70.00) 180.00

#World info: 180 lines, 1270 points (=> 228,600 requests, 99 days, 13 points/day)

#Implementation:

# google2.create_both_images('World 0',71.30,-180.00, 179, 13) #sa includa si nordul norvegiei
# google2.create_both_images('World 1',70.00,-180.00, 179, 13)
# google2.create_both_images('World 2',68.70,-180.00, 179, 13)
# google2.create_both_images('World 3',67.40,-180.00, 179, 13)
# google2.create_both_images('World 4',66.10,-180.00, 179, 13)
# google2.create_both_images('World 5',64.80,-180.00, 179, 13)
# google2.create_both_images('World 6',63.50,-180.00, 179, 13)
# google2.create_both_images('World 7',62.20,-180.00, 179, 13)
# google2.create_both_images('World 8',60.90,-180.00, 179, 13)
# google2.create_both_images('World 9',59.60,-180.00, 179, 13)
# google2.create_both_images('World 10',58.30,-180.00, 179, 13)
# google2.create_both_images('World 11',57.00,-180.00, 179, 13)
# google2.create_both_images('World 12',55.70,-180.00, 179, 13)
# google2.create_both_images('World 13',54.40,-180.00, 179, 13)
# google2.create_both_images('World 14',53.10,-180.00, 179, 13)
# google2.create_both_images('World 15',51.80,-180.00, 179, 13)
# google2.create_both_images('World 16',50.50,-180.00, 179, 13)
# google2.create_both_images('World 17',49.20,-180.00, 179, 13)
# google2.create_both_images('World 18',47.90,-180.00, 179, 13)
# google2.create_both_images('World 19',46.60,-180.00, 179, 13)
# google2.create_both_images('World 20',45.30,-180.00, 179, 13)
# google2.create_both_images('World 21',44.00,-180.00, 179, 13)
# google2.create_both_images('World 22',42.70,-180.00, 179, 13)
# google2.create_both_images('World 23',41.40,-180.00, 179, 13)
# google2.create_both_images('World 24',40.10,-180.00, 179, 13)
# google2.create_both_images('World 25',38.80,-180.00, 179, 13)
# google2.create_both_images('World 26',37.50,-180.00, 179, 13)
# google2.create_both_images('World 27',36.20,-180.00, 179, 13)
# google2.create_both_images('World 28',34.90,-180.00, 179, 13)
# google2.create_both_images('World 29',33.60,-180.00, 179, 13)
# google2.create_both_images('World 30',32.30,-180.00, 179, 13)
# google2.create_both_images('World 31',31.00,-180.00, 179, 13)
# google2.create_both_images('World 32',29.70,-180.00, 179, 13)
# google2.create_both_images('World 33',28.40,-180.00, 179, 13)
# google2.create_both_images('World 34',27.10,-180.00, 179, 13)
# google2.create_both_images('World 35',25.80,-180.00, 179, 13)
# google2.create_both_images('World 36',24.50,-180.00, 179, 13)
# google2.create_both_images('World 37',23.20,-180.00, 179, 13)
# google2.create_both_images('World 38',21.90,-180.00, 179, 13)
# google2.create_both_images('World 39',70.00,-180.00, 179, 13)
# google2.create_both_images('World 40',20.60,-180.00, 179, 13)
# google2.create_both_images('World 41',19.30,-180.00, 179, 13)
# google2.create_both_images('World 42',18.00,-180.00, 179, 13)
# google2.create_both_images('World 43',16.70,-180.00, 179, 13)
# google2.create_both_images('World 44',15.40,-180.00, 179, 13)
# google2.create_both_images('World 45',14.10,-180.00, 179, 13)
# google2.create_both_images('World 46',12.80,-180.00, 179, 13)
# google2.create_both_images('World 47',11.50,-180.00, 179, 13)
# google2.create_both_images('World 48',10.20,-180.00, 179, 13)
# google2.create_both_images('World 49',8.90,-180.00, 179, 13)
# google2.create_both_images('World 50',7.60,-180.00, 179, 13)
# google2.create_both_images('World 51',6.30,-180.00, 179, 13)
# google2.create_both_images('World 52',5.00,-180.00, 179, 13)
# google2.create_both_images('World 53',3.70,-180.00, 179, 13)
# google2.create_both_images('World 54',2.40,-180.00, 179, 13)
# google2.create_both_images('World 55',1.10,-180.00, 179, 13)
# google2.create_both_images('World 56',-0.20,-180.00, 179, 13)
# google2.create_both_images('World 57',-1.50,-180.00, 179, 13)
# google2.create_both_images('World 58',-2.80,-180.00, 179, 13)
# google2.create_both_images('World 59',-4.10,-180.00, 179, 13)
# google2.create_both_images('World 60',-5.40,-180.00, 179, 13)
# google2.create_both_images('World 61',-6.70,-180.00, 179, 13)
# google2.create_both_images('World 62',-8.00,-180.00, 179, 13)
# google2.create_both_images('World 63',-9.30,-180.00, 179, 13)
# google2.create_both_images('World 64',-10.60,-180.00, 179, 13)
# google2.create_both_images('World 65',-11.90,-180.00, 179, 13)
# google2.create_both_images('World 66',-13.20,-180.00, 179, 13)
# google2.create_both_images('World 67',-14.50,-180.00, 179, 13)
# google2.create_both_images('World 68',-15.80,-180.00, 179, 13)
# google2.create_both_images('World 69',-17.10,-180.00, 179, 13)
# google2.create_both_images('World 70',-18.40,-180.00, 179, 13)
# google2.create_both_images('World 71',-19.70,-180.00, 179, 13)
# google2.create_both_images('World 72',-21.00,-180.00, 179, 13)
# google2.create_both_images('World 73',-22.30,-180.00, 179, 13)
# google2.create_both_images('World 74',-23.60,-180.00, 179, 13)
# google2.create_both_images('World 75',-24.90,-180.00, 179, 13)
# google2.create_both_images('World 76',-26.20,-180.00, 179, 13)
# google2.create_both_images('World 77',-27.50,-180.00, 179, 13)
# google2.create_both_images('World 78',-28.80,-180.00, 179, 13)
# google2.create_both_images('World 79',-30.10,-180.00, 179, 13)
# google2.create_both_images('World 80',-31.40,-180.00, 179, 13)
# google2.create_both_images('World 81',-32.70,-180.00, 179, 13)
# google2.create_both_images('World 82',-34.00,-180.00, 179, 13)
# google2.create_both_images('World 83',-35.30,-180.00, 179, 13)
# google2.create_both_images('World 84',-36.60,-180.00, 179, 13)
# google2.create_both_images('World 85',-37.90,-180.00, 179, 13)
# google2.create_both_images('World 86',-39.20,-180.00, 179, 13)
# google2.create_both_images('World 87',-40.50,-180.00, 179, 13)
# google2.create_both_images('World 88',-41.80,-180.00, 179, 13)
# google2.create_both_images('World 89',-43.10,-180.00, 179, 13)
# google2.create_both_images('World 90',-44.40,-180.00, 179, 13)
# google2.create_both_images('World 91',-45.70,-180.00, 179, 13)
# google2.create_both_images('World 92',-47.00,-180.00, 179, 13)
# google2.create_both_images('World 93',-48.30,-180.00, 179, 13)
# google2.create_both_images('World 94',-49.60,-180.00, 179, 13)
# google2.create_both_images('World 95',-50.90,-180.00, 179, 13)
# google2.create_both_images('World 96',-52.20,-180.00, 179, 13)
# google2.create_both_images('World 97',-53.50,-180.00, 179, 13)
# google2.create_both_images('World 98',-54.80,-180.00, 179, 13)
# google2.create_both_images('World 99',-56.10,-180.00, 179, 13)


########################################################## WORLD CVS ###################################################
path = r"C:\Users\Andrei\Desktop\de-ale mele\elevation map project\srtm_1km.asc"

# bing.append_elevations_to_cvs(path, 71.30, -180.00, 48, 68) 
# bing.append_elevations_to_cvs(path, 69.94, -180.00, 48, 68) 
# bing.append_elevations_to_cvs(path, 68.58, -180.00, 48, 68) 
# bing.append_elevations_to_cvs(path, 67.22, -180.00, 48, 68) 
# bing.append_elevations_to_cvs(path, 65.86, -180.00, 48, 68) 
# bing.append_elevations_to_cvs(path, 64.50, -180.00, 48, 68) 
# bing.append_elevations_to_cvs(path, 63.14, -180.00, 48, 68) 
# bing.append_elevations_to_cvs(path, 61.78, -180.00, 48, 68) 
# bing.append_elevations_to_cvs(path, 60.42, -180.00, 48, 68) 
# bing.append_elevations_to_cvs(path, 59.06, -180.00, 48, 68) 
# bing.append_elevations_to_cvs(path, 57.70, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 56.34, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 54.98, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 53.62, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 52.26, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 50.90, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 49.54, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 48.18, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 46.82, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 45.46, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 44.10, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 42.74, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 41.38, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 40.02, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 38.66, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 37.30, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 35.94, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 33.22, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 31.86, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 30.50, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 29.14, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 27.78, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 26.42, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 25.06, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 23.70, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 22.34, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 20.98, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 19.62, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 18.26, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 16.90, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 15.54, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 14.18, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 12.82, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 11.46, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 10.10, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 8.74, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 7.38, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 6.02, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 4.66, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 3.30, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 1.94, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, 0.58, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -0.78, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -2.14, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -3.50, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -4.86, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -6.22, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -7.58, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -8.94, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -10.30, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -11.16, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -13.20, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -14.38, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -15.74, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -17.10, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -18.46, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -19.82, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -21.18, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -22.54, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -23.90, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -25.26, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -26.62, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -27.98, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -29.34, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -30.70, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -32.06, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -33.42, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -34.78, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -36.14, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -37.50, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -38.86, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -40.22, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -41.58, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -42.94, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -44.30, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -45.66, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -47.02, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -48.38, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -49.74, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -51.10, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -52.46, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -53.82, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -55.18, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -56.54, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -57.90, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -59.26, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -60.62, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -61.98, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -63.34, -180.00, 48, 68)
# bing.append_elevations_to_cvs(path, -64.70, -180.00, 48, 68)

asc_to_image.elevations_asc_to_relief_image(path, "World_relief")
asc_to_image.elevations_asc_to_rugedness_image(path, "World_rugedness")

# from google.bing import line_to_str
# 
# line = [(35.894309002906084, -110.72522000409663),
#         (35.893930979073048, -110.72577999904752),
#         (35.893744984641671, -110.72606003843248),
#         (35.893366960808635, -110.72661500424147)]
# 
# print(line_to_str(line))
# j = line_to_str(line)
# print(j == "vx1vilihnM6hR7mEl2Q")
