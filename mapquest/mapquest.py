import json
from urllib.request import urlopen

from numpy.core.function_base import linspace


def mean_elevation_for_point( start_latitude:float, start_longitude:float, l=10) ->float:
    """
    This function coputes the main elevation for a square area using Google Maps Elevation API
    :param start_latitude: the latitude of the top left corner of the square (negative means south)
    :param start_longitude: the longitude of the top left corner of the square (negative means west)
    :param l: the lenght of the square side, in kilometers
    :return: the mean elevation
    """
    equidistant_distances = linspace(0,l/100,5)
    equidistant_longitudes = equidistant_distances + start_longitude
    equidistant_latitudes = equidistant_distances*(-1) + start_latitude
    elevation_sum = 0
    for longitude in equidistant_longitudes:
        for latitude in equidistant_latitudes:
            s = "https://maps.googleapis.com/maps/api/elevation/json?locations=" + str(latitude) + "," + str(longitude) + "&key=AIzaSyC0j9OEnXQ99rqEjgiVvrpFhMX5KQ5iFYk"
            content = urlopen(s).read()

            content = json.loads(content.decode("utf-8"))

            elevation = content['results'][0]['elevation']
            # print(latitude, longitude, elevation)
            elevation_sum += elevation

    return elevation_sum/25

s = "http://open.mapquestapi.com/elevation/v1/profile?key=gnBVbDS3Ydtgjeu0ltrY3qSVXvAAHJsQ&shapeFormat=raw&latLngCollection=56.47,23.49,56.47,23.5,56.47,23.51,56.47,23.52,56.47,23.53"
content = urlopen(s).read()

content = json.loads(content.decode("utf-8"))
print(content)

# elevation = content['results'][0]['elevation']
# print(latitude, longitude, elevation)
# elevation_sum += elevation